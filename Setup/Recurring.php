<?php

namespace Survey\SurveyPage\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class Recurring implements InstallSchemaInterface
{
    const ANSWER_TABLE = 'survey_answer';
    const ANSWER_ID = 'answer_id';

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $coreResource;

    /**
     * @param \Magento\Framework\App\ResourceConnection $coreResource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $coreResource
    ) {
        $this->coreResource = $coreResource;
    }

    /**
     * Stub for recurring setup script.
     *
     * For quick development and prototyping purposes we re-create our tables during every call to setup:upgrade,
     * regardless of version upgrades, which you normally would not do in production.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $setup
     * @throws
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $this->coreResource->getConnection();

        // Drop existing tables for quick prototyping. Comment out if you don't want this!
        if ($connection->isTableExists(self::ANSWER_TABLE)) {
            $connection->dropTable(self::ANSWER_TABLE);
        }

        $surveyTable = $connection->newTable(
            self::ANSWER_TABLE
        )->addColumn(
            self::ANSWER_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Answer ID'

            // build up your DB schema here
        )->addColumn(
            'product_rating',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Product Rating'    
        )->addColumn(
            'website_rating',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Website Rating'    
        )->addColumn(
            'delivery_rating',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Delivery Rating'    
        )->addColumn(
            'recommendation_rating',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Recommendate'    
        )->addColumn(
            'finding_selection',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Found by'    
        )->addColumn(
            'comment',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Comment'    
        )->setComment('Webshop General Survey');

        $connection->createTable($surveyTable);

        $setup->endSetup();
    }
}