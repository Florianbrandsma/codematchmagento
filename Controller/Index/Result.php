<?php

namespace Survey\SurveyPage\Controller\Index;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Element\Messages;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\View\Result\Page;

class Result extends Action
{
    protected $resultPageFactory;
    
     public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;
        parent::__construct($context);
    }
    
    
     public function execute()
    {
  
         $comment = $this->getRequest()->getPost('comment');
         echo $comment;
         
         $resultPage = $this->resultPageFactory->create();
         
         
        $messageBlock = $resultPage->getLayout()->createBlock(
            'Magento\Framework\View\Element\Messages',
            'answer'
        );
        
        
        $messageBlock->addSuccess("Success!");
 
        $resultPage->getLayout()->setChild(
            'content',
            $messageBlock->getNameInLayout(),
            'answer_alias'
        );
 
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
     }
}
