<?php

namespace Survey\SurveyPage\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use \Survey\SurveyPage\Api\Data\AnswerInterface;

/**
 * Class File
 * @package Toptal\Blog\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Answer extends AbstractModel implements AnswerInterface, IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'survey_answer';

    /**
     * Post Initialization
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Survey\SurveyPage\Model\ResourceModel\Answer');
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ANSWER_ID);
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * Get Title
     *
     * @return string|null
     */
    public function getProductRating()
    {
        return $this->getData(self::PRODUCT_RATING);
    }

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getWebsiteRating()
    {
        return $this->getData(self::WEBSITE_RATING);
    }

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getDeliveryRating()
    {
        return $this->getData(self::DELIVERY_RATING);
    }
    
    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getRecommendationRating()
    {
        return $this->getData(self::RECOMMENDATION_RATING);
    }

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getFindingSelection()
    {
        return $this->getData(self::FINDING_SELECTION);
    }
    
    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->getData(self::COMMENT);
    }
    
    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ANSWER_ID, $id);
    }
    
    /**
     * Set Title
     *
     * @param string $title
     * @return $this
     */
    public function setProductRating($rating)
    {
        return $this->setData(self::PRODUCT_RATING, $rating);
    }

    /**
     * Set Content
     *
     * @param string $content
     * @return $this
     */
    public function setWebsiteRating($rating)
    {
        return $this->setData(self::WEBSITE_RATING, $rating);
    }

    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setDeliveryRating($rating)
    {
        return $this->setData(self::DELIVERY_RATING, $rating);
    }
    
    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setRecommendationRating($rating)
    {
        return $this->setData(self::RECOMMENDATION_RATING, $rating);
    }
    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setFindingSelection($selection)
    {
        return $this->setData(self::FINDING_SELECTION, $selection);
    }
    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }
}