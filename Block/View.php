<?php

namespace Survey\SurveyPage\Block;

use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Registry;
use \Survey\SurveyPage\Model\Answer;
use \Survey\SurveyPage\Model\AnswerFactory; //Does not exist
use \Survey\SurveyPage\Controller\Answer\View as ViewAction;

class View extends Template
{
    /**
     * Core registry
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Answer
     * @var null|Answer
     */
    protected $_answer = null;

    /**
     * AnswerFactory
     * @var null|AnswerFactory
     */
    protected $_answerFactory = null;

    /**
     * Constructor
     * @param Context $context
     * @param Registry $coreRegistry
     * @param AnswerFactory $answerCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        AnswerFactory $answerFactory,
        array $data = []
    ) {
        $this->_answerFactory = $answerFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * Lazy loads the requested answer
     * @return Answer
     * @throws LocalizedException
     */
    public function getAnswer()
    {
        if ($this->_answer === null) {
            /** @var Answer $answer */
            $answer = $this->_answerFactory->create();
            $answer->load($this->_getAnswerId());

            if (!$answer->getId()) {
                throw new LocalizedException(__('Answer not found. This is a test'));
            }

            $this->_answer = $answer;
        }
        return $this->_answer;
    }

    /**
     * Retrieves the answer id from the registry
     * @return int
     */
    protected function _getAnswerId()
    {
        return (int) $this->_coreRegistry->registry(
            ViewAction::REGISTRY_KEY_POST_ID
        );
    }
}