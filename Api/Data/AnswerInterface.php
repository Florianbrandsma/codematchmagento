<?php

namespace Survey\SurveyPage\Api\Data;

interface AnswerInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ANSWER_ID             = 'answer_id';
    const PRODUCT_RATING        = 'product_rating';
    const WEBSITE_RATING        = 'website_rating';
    const DELIVERY_RATING       = 'delivery_rating';
    const RECOMMENDATION_RATING = 'recommendation_rating';
    const FINDING_SELECTION     = 'finding_selection';
    const COMMENT               = 'comment';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();
    
    /**
     * Get Title
     *
     * @return string|null
     */
    public function getProductRating();

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getWebsiteRating();

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getDeliveryRating();

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getRecommendationRating();
    
    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getFindingSelection();
    
    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getComment();
    
    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);
    
    /**
     * Set Title
     *
     * @param string $title
     * @return $this
     */
    public function setProductRating($rating);

    /**
     * Set Content
     *
     * @param string $content
     * @return $this
     */
    public function setWebsiteRating($rating);

    /**
     * Set Crated At
     *
     * @param int $createdAt
     * @return $this
     */
    public function setDeliveryRating($rating);
    
    /**
     * Set Crated At
     *
     * @param int $createdAt
     * @return $this
     */
    public function setRecommendationRating($rating);
    
    /**
     * Set Crated At
     *
     * @param int $createdAt
     * @return $this
     */
    public function setFindingSelection($selection);
    
    /**
     * Set Crated At
     *
     * @param int $createdAt
     * @return $this
     */
    public function setComment($comment);
}

